// Computing 2 Lab Intro 1.cpp :
//
#include <iostream>
#include <string>
#include <vector>


using namespace std;

int main() {

	string name;
	vector<int> bike; // Motocross engine size cc
	unsigned int total;
	int cc;

	cout << "What is your name? \n";
	cin >> name;
	cout << "How many bikes do you have? \n";
	cin >> total;

	cout << "Enter your engine sizes below \n";

	for (unsigned int i = 0; i < total; i++) {

		cin >> cc;

		bike.push_back(cc);

	}

	cout << name << ", your bike engines are: \n";

	for (int v : bike) {
		cout << v << "cc" << endl;
	}

	return 0;

}
